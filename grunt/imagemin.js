'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
      files: [{
        expand: true,
        cwd: data.appPath + '/images',
        src: '**/{,*/}*.{png,jpg,jpeg,gif}',
        dest: data.distPath + '/images'
      }]
    }
  };
};