# CapSo frontend developer test

## Overview
Welcome to CapSo. 
This project is designed to resemble our development environment with some realistic dummy data.
We have outlined a few tasks to help you demonstrate the scope of your experience.
Please interpret the requirements freely and use the libraries and frameworks you're familiar with.
Further instructions will be available once you serve locally. Good luck!

## Setup

Install all required Node modules
```bash
npm install  
```

Install all required Bower components
```bash
bower install  
```

Update webdriver manager
```bash
./node_modules/grunt-protractor-runner/scripts/webdriver-manager-update  
```

## Running the project and testing

To start the project run
```bash
grunt serve:beta  
```

To run unit tests suites use
```bash
grunt test
```

To start end-to-end Protractor test run
```bash
grunt e2e
```