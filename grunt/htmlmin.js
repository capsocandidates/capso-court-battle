'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
      options: {
        collapseWhitespace: true,
        conservativeCollapse: true,
        collapseBooleanAttributes: true,
        removeCommentsFromCDATA: true,
        removeOptionalTags: true
      },
      files: [{
        expand: true,
        cwd: data.distPath + '',
        src: ['*.html', '**/*.html'],
        dest: data.distPath + ''
      }]
    }
  };
};