'use strict';

module.exports = function (grunt, data) {
  return {
    options: {
      browsers: ['last 1 version']
    },
    dist: {
      files: [{
        expand: true,
        cwd: data.rootPath + '/.tmp/styles/',
        src: '{,*/}*.css',
        dest: data.rootPath + '/.tmp/styles/'
      }]
    }
  };
};