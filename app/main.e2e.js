'use strict';

/* global $ */

describe('test-overview', function() {

    it('should navigate to test-overview page', function() {

        browser.get('/test-overview');
        expect(browser.getCurrentUrl()).toBe('http://localhost:9000/test-overview');

    });

    it('should navigate to test-overview page again and read the value of `h1` tag', function() {

        browser.get('/test-overview');
        expect($('h1').getText()).toBe('This string breaks this test, remember to e2e test your work with Protractor');

    });


});