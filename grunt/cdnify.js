'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
        html: [data.distPath + '/*.html']
    }
  };
};