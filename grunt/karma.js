'use strict';

module.exports = function (grunt, data) {
    return {
        unit: {
            configFile: data.rootPath + '/karma.conf.js',
            //NOTE: Grunt watch runs this task every JS file change, so this task needs to end (singleRun: true) to allow subsequent tasks to run
            singleRun: true,
        }
    };
};