'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
      files: [{
        dot: true,
        src: [
          data.rootPath + '/.tmp',
          data.distPath + '/{,*/}*',
          '!' + data.distPath +'/.git{,*/}*'
        ]
      }]
    },
    server: data.rootPath + '/.tmp'
  };
};