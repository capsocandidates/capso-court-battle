'use strict';

module.exports = function (grunt, data) {
  return {
    html2js: {
      files: [
        data.appPath + '/**/*.html',
      ],
      tasks: ['newer:html2js:main']
    },
    bower: {
      files: [data.rootPath + '/bower.json'],
      tasks: ['wiredep']
    },
    js: {
      files: [
        data.appPath + '/**/*.js', 
        '!' + data.appPath + '/**/*.spec.js',
        '!' + data.appPath + '/**/*.e2e.js'
      ],
      tasks: ['newer:jshint:all'],
      options: {
        livereload: '<%= connect.options.livereload %>'
      }
    },
    jsTest: {
      files: [
        data.appPath + '/**/*.js', 
        data.rootPath + '/test/karma.conf.js',
        '!' + data.appPath + '/**/*.e2e.js',
        '!' + data.appPath + '/**/*.e2e-page.js'
      ],
      tasks: ['newer:jshint:test', 'karma']
    },
    sass: {
      files: [data.appPath + '/**/*.{scss,sass}'],
      tasks: ['sass:dist']
    },
    gruntfile: {
      files: [data.rootPath + '/Gruntfile.js', data.rootPath + '/grunt/*.js'],
    },
    e2e: {
      files: [
        data.rootPath + '/**/*.e2e.js',
        data.rootPath + '/**/*.e2e-page.js'
      ],
      tasks: ['e2e'],
    },
    livereload: {
      options: {
        livereload: '<%= connect.options.livereload %>'
      },
      files: [
        data.appPath + '/**/*.html',
        data.appPath + '/**/*.js',
        '!' + data.appPath + '/**/*.e2e.js',
        '!' + data.appPath + '/**/*.e2e-page.js',
        data.rootPath + '/**/*.scss',
        data.appPath + '/images/**/*'
      ]
    }
  };
};