'use strict';

// Package configs are in separate files located in "grunt/" folder; FILE_NAME.js must match the corresponding Grunt package name
// All tasks are defined in "grunt/aliases.js"

module.exports = function (grunt) {

  var path = require('path');
  var rootPath = process.cwd();
  var bowerAppPath = require('./bower.json').appPath || 'app';

  var options  = {
    init: true,
    configPath: path.join(rootPath, 'grunt'),
    jitGrunt: {
      staticMappings: {
        useminPrepare: 'grunt-usemin',
        cdnify: 'grunt-google-cdn',
        protractor:'grunt-protractor-runner'
      }
    },
    data: {
      rootPath: '.',
      appPath: bowerAppPath,
      distPath: 'dist',
      angularCapsoAppBaseModuleName: 'capsoTestApp',
      specFileLocation: rootPath + '/app/scripts/**/*.spec.js',
      modRewrite  : require('connect-modrewrite'),
      e2eConfigFileLocation: rootPath + '/protractor.config.js',
      deploymentConfig: {
        fileName:'grunt-copy__deployment-environment.config.js',
        filePath: {
          beta: rootPath + '/angular-deployment-config/beta',
          live: rootPath + '/angular-deployment-config/live',
          feature: rootPath + '/angular-deployment-config/feature',
        },
      }
    }
  };
  require('time-grunt')(grunt);
  require('load-grunt-config')(grunt, options);
};
