'use strict';

module.exports = function (grunt, data) {
  return {
    dist: {
      files: [{
        expand: true,
        dot: true,
        cwd: data.appPath,
        dest: data.distPath,
        src: [
          '*.{ico,png,txt}',
          '.htaccess',
          '*.html',
          '**/*.html',
          'images/*.{webp,webm}',
          'fonts/**/*',
          'dummy/*.json'
        ]
      }, {
        expand: true,
        cwd: data.rootPath + '/.tmp/images',
        dest: data.distPath + '/images',
        src: ['generated/*']
      }]
    },
    styles: {
      expand: true,
      cwd: data.appPath + '/styles',
      dest: data.rootPath + '/.tmp/styles/',
      src: '{,*/}*.css'
    },
    betaDeploymentConfig: {
      expand: true,
      cwd: data.deploymentConfig.filePath.beta,
      src: data.deploymentConfig.fileName,
      dest: data.appPath,
    },
    liveDeploymentConfig: {
      expand: true,
      cwd: data.deploymentConfig.filePath.live,
      src: data.deploymentConfig.fileName,
      dest: data.appPath,
    },
    featureDeploymentConfig: {
      expand: true,
      cwd: data.deploymentConfig.filePath.feature,
      src: data.deploymentConfig.fileName,
      dest: data.appPath,
    }
  };
};