(function() {
'use strict';


angular
.module('capsoTestApp')
.config(MainConfig);


MainConfig.$inject=['$locationProvider'];
function MainConfig( $locationProvider ) {

    $locationProvider.html5Mode(true);

}


})();