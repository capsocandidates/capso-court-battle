/*'use strict';

module.exports = function (grunt, data) {
  console.log(data.e2eConfigFileLocation);
  return {
    options: {
      configFile: 'node_modules/protractor/example/conf.js', // Default config file 
      keepAlive: false, // If false, the grunt process stops when the test fails. 
      noColor: false, // If true, protractor will not use colors in its output. 
      args: {
        // Arguments passed to the command 
      },
      //debug:true,
      verbose: true,
    },
    all: {   // Grunt requires at least one target to run so you can simply put 'all: {}' here too. 
      options: {
        //verbose: true,
        configFile: data.e2eConfigFileLocation, 
        args: {}, // Target-specific arguments 
      }
    },
  };
};*/

'use strict';


module.exports = function (grunt, data) {
    return {
        options: {
            keepAlive: false,
            configFile: 'protractor.config.js'
        },
        singlerun: {},
        auto: {
            keepAlive: true,
            options: {
                args: {
                    seleniumPort: 4444
                }
            }
        },
        allScriptsTimeout: 5000,
    };
};