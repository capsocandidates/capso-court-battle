'use strict';

module.exports = function (grunt, data) {
  return {
    options: {
      base:'app',
      module: data.angularCapsoAppBaseModuleName + '.gruntHtml2jsAngularTemplates'
    },
    main: {
      src: [
        data.appPath + '/scripts/**/*.html',
        data.appPath + '/views/**/*.html',
        '!index.html',
        '!404.html'
      ],
      dest: data.appPath + '/gruntHtml2jsAngularTemplates.module.js'
    }
  };
};