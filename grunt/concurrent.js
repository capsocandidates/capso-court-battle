'use strict';

module.exports = function (grunt, data) {
  return {
    local: ['sass:dist'],
    dist: ['sass:dist','imagemin','svgmin']
  };
};