'use strict';

module.exports = function (grunt, data) {
  return {
    html: [data.distPath + '/**/*.html'],
    css: [data.distPath + '/styles/**/*.css'],
    js: data.distPath + '/scripts/*.js',
    options: {
      assetsDirs: [
        data.distPath + '',
        data.distPath + '/images'
      ],
      patterns: {
        js: [
            [/(images\/.*?\.(?:gif|jpeg|jpg|png|webp))/gm, 'Update the JS to reference our revved images']
        ]
      }
    }
  };
};