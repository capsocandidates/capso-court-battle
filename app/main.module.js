(function() {
'use strict';



angular.module('capsoTestApp', [
	'ui.router',
	'ngAnimate',
	'ngResource',
	'ngSanitize',
	'ngTouch',
    'capsoTestApp.gruntHtml2jsAngularTemplates'
]);



})();